const { override, overrideDevServer } = require('customize-cra');
const path = require('path');
const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');

function traverse(obj, callback) {
  if (Array.isArray(obj)) {
    obj.forEach((item) => traverse(item, callback));
  } else if (typeof obj === 'object' && obj !== null) {
    Object.keys(obj).forEach((key) => {
      if (obj.hasOwnProperty(key)) {
        callback(obj, key);
        traverse(obj[key], callback);
      }
    });
  }
}

module.exports = {
  webpack: override(
    function (config) {

      config.resolve = {
        ...config.resolve,
        alias: { '@': path.resolve(__dirname, 'src') },
      };

      config.module.rules.push({
        test: /\.css$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'src'),
        use: [
          // No need for "css-loader" nor "style-loader"
          // for CRA will later apply them anyways.
          {
            loader: "postcss-loader",
            options: {
              sourceMap: true,
              postcssOptions: {
                indent: 'postcss',
                plugins: [
                  tailwindcss('./tailwind.config.js'),
                  autoprefixer()
                ]
              }
            }
          }
        ],
      });
    
      traverse(config, (node, key) => {
        if (key === 'loader') {
          if (
            node[key].indexOf('sass-loader') !== -1 ||
            node[key].indexOf('postcss-loader') !== -1 ||
            node[key].indexOf('css-loader') !== -1
          ) {
            if (node.options) {
              node.options.sourceMap = true;
            }
          }
        }
      });
      return config;
    }
  ),
  devServer: overrideDevServer(
    function (devServer) {
      devServer.port = 3090;
      devServer.host = 'local.tracorona.com';
      return devServer
    }
  )
};