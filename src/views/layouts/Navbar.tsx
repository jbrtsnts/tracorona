import React, { Component, MouseEvent } from 'react';
import { ClassNames } from '@/views/layouts/StyledComponents';
import {
  Alarm,
  Collections,
  Widgets,
  ExpandMore,
  AllInbox,
  CommentBank,
  ViewList,
  Event,
  Face,
  Inbox,
  Verified,
  Mail,
  ExitToApp,
  Notifications,
  MenuOpen,
  AccountCircle,
} from '@styled-icons/material-twotone';

import LogoSrc from '../../assets/images/logo.png';
import User1Src from '../../assets/images/user1.jpg';

type NavbarState = {
  notifications: boolean;
  messages: boolean;
  myaccount: boolean;
  [propName: string]: any;
};
export default class Navbar extends Component<{}, NavbarState> {
  private consoleLog = (event: MouseEvent): void => {
    event.preventDefault();
    console.log(event);
  };

  state: NavbarState = {
    notifications: false,
    messages: false,
    myaccount: false,
  };

  toggleProp(stateProp: string): void {
    console.log(this);
    this.setState({
      [stateProp]: !this.state[stateProp],
    });
  }

  public render(): JSX.Element {
    const { consoleLog } = this;
    return (
      <div className={ClassNames.header}>
        <div className="flex-none w-56 flex flex-row items-center">
          <img src={LogoSrc} className="w-10 flex-none" alt="Logo" />
          <strong className="capitalize ml-1 flex-1">tracorona</strong>

          <button
            id="sliderBtn"
            className="flex-none text-right text-gray-900 hidden md:block"
          >
            <MenuOpen />
          </button>
        </div>

        <button
          id="navbarToggle"
          className="hidden md:block md:fixed right-0 mr-6"
        >
          <ExpandMore />
        </button>

        <div
          id="navbar"
          className={ClassNames.navbar}
        >
          <div className="flex text-gray-600 md:w-full md:flex md:flex-row md:justify-evenly md:pb-10 md:mb-10 md:border-b md:border-gray-200">

            <a
              className="mr-2 transition duration-500 ease-in-out hover:text-gray-900"
              href="#body"
              onClick={consoleLog}
              title="email"
            >
              <AllInbox className="h-6 w-6" />
            </a>
            <a
              className="mr-2 transition duration-500 ease-in-out hover:text-gray-900"
              href="#body"
              onClick={consoleLog}
              title="email"
            >
              <CommentBank className="h-6 w-6" />
            </a>
            <a
              className="mr-2 transition duration-500 ease-in-out hover:text-gray-900"
              href="#body"
              onClick={consoleLog}
              title="email"
            >
              <Verified className="h-6 w-6" />
            </a>
            <a
              className="mr-2 transition duration-500 ease-in-out hover:text-gray-900"
              href="#body"
              onClick={consoleLog}
              title="email"
            >
              <Event className="h-6 w-6" />
            </a>
          </div>

          <div className="flex flex-row-reverse items-center">
            <div className="dropdown relative md:static">
              <button
                onClick={() => this.toggleProp('myaccount')}
                className="menu-btn focus:outline-none focus:shadow-outline flex flex-wrap items-center"
              >
                <div className="w-8 h-8 overflow-hidden rounded-full">
                  <AccountCircle className="w-full h-full object-cover" />
                </div>

                <div className="ml-2 capitalize flex ">
                  <h1 className="text-sm text-gray-800 font-semibold m-0 p-0 leading-none">
                    moeSaid
                  </h1>
                  <ExpandMore className="h-4 w-4 ml-1 text-xs leading-none" />
                </div>
              </button>

              <button
                onClick={() => this.toggleProp('myaccount')}
                className={`fixed top-0 left-0 z-10 w-full h-full menu-overflow ${
                  this.state.myaccount ? '' : 'hidden'
                }`}
              ></button>

              <div
                className={`text-gray-500 menu md:mt-10 md:w-full rounded bg-white shadow-md absolute z-20 right-0 w-40 mt-5 py-2 animated faster ${
                  this.state.myaccount ? 'fadeIn' : 'hidden'
                }`}
              >
                <a
                  className="px-4 py-2 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 hover:text-gray-900 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <Face className="h-5 w-5 mr-2" />
                  edit my profile
                </a>

                <a
                  className="px-4 py-2 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 hover:text-gray-900 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <Inbox className="h-5 w-5 mr-2" />
                  my inbox
                </a>

                <a
                  className="px-4 py-2 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 hover:text-gray-900 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <ViewList className="h-5 w-5 mr-2" />
                  tasks
                </a>

                <a
                  className="px-4 py-2 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 hover:text-gray-900 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <Mail className="h-5 w-5 mr-2" />
                  chats
                </a>

                <hr />

                <a
                  className="px-4 py-2 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 hover:text-gray-900 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <ExitToApp className="h-5 w-5 mr-2" />
                  log out
                </a>
              </div>
            </div>

            <div className="dropdown relative mr-5 md:static">
              <button
                onClick={() => this.toggleProp('notifications')}
                className="text-gray-500 menu-btn p-0 m-0 hover:text-gray-900 focus:text-gray-900 focus:outline-none transition-all ease-in-out duration-300"
              >
                <Notifications className="h-6 w-6" />
              </button>

              <button
                onClick={() => this.toggleProp('notifications')}
                className={`fixed top-0 left-0 z-10 w-full h-full menu-overflow ${
                  this.state.notifications ? '' : 'hidden'
                }`}
              ></button>

              <div
                className={`menu rounded bg-white md:right-0 md:w-full shadow-md absolute z-20 right-0 w-84 mt-5 py-2 animated faster ${
                  this.state.notifications ? 'fadeIn' : 'hidden'
                }`}
              >
                <div className="px-4 py-2 flex flex-row justify-between items-center capitalize font-semibold text-sm">
                  <h1>notifications</h1>
                  <div className="bg-teal-100 border border-teal-200 text-teal-500 text-xs rounded px-1">
                    <strong>5</strong>
                  </div>
                </div>
                <hr />

                <a
                  className="flex flex-row items-center justify-start px-4 py-4 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <div className="px-3 py-2 rounded mr-3 bg-gray-100 border border-gray-300">
                    <Widgets className="h-4 w-4 text-gray-600" />
                  </div>

                  <div className="flex-1 flex flex-rowbg-green-100">
                    <div className="flex-1">
                      <h1 className="text-sm font-semibold">poll..</h1>
                      <p className="text-xs text-gray-500">text here also</p>
                    </div>
                    <div className="text-right text-xs text-gray-500">
                      <p>4 min ago</p>
                    </div>
                  </div>
                </a>
                <hr />

                <a
                  className="flex flex-row items-center justify-start px-4 py-4 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <div className="px-3 py-2 rounded mr-3 bg-gray-100 border border-gray-300">
                    <AccountCircle className="h-4 w-4 text-gray-600" />
                  </div>

                  <div className="flex-1 flex flex-rowbg-green-100">
                    <div className="flex-1">
                      <h1 className="text-sm font-semibold">mohamed..</h1>
                      <p className="text-xs text-gray-500">text here also</p>
                    </div>
                    <div className="text-right text-xs text-gray-500">
                      <p>78 min ago</p>
                    </div>
                  </div>
                </a>
                <hr />

                <a
                  className="flex flex-row items-center justify-start px-4 py-4 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <div className="px-3 py-2 rounded mr-3 bg-gray-100 border border-gray-300">
                    <Collections className="h-4 w-4 text-gray-600" />
                  </div>

                  <div className="flex-1 flex flex-rowbg-green-100">
                    <div className="flex-1">
                      <h1 className="text-sm font-semibold">new imag..</h1>
                      <p className="text-xs text-gray-500">text here also</p>
                    </div>
                    <div className="text-right text-xs text-gray-500">
                      <p>65 min ago</p>
                    </div>
                  </div>
                </a>
                <hr />

                <a
                  className="flex flex-row items-center justify-start px-4 py-4 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <div className="px-3 py-2 rounded mr-3 bg-gray-100 border border-gray-300">
                    <Alarm className="h-4 w-4 text-gray-600" />
                  </div>

                  <div className="flex-1 flex flex-rowbg-green-100">
                    <div className="flex-1">
                      <h1 className="text-sm font-semibold">time is up..</h1>
                      <p className="text-xs text-gray-500">text here also</p>
                    </div>
                    <div className="text-right text-xs text-gray-500">
                      <p>1 min ago</p>
                    </div>
                  </div>
                </a>

                <hr />
                <div className="px-4 py-2 mt-2">
                  <a
                    href="#body"
                    onClick={consoleLog}
                    className="border border-gray-300 block text-center text-xs uppercase rounded p-1 hover:text-teal-500 transition-all ease-in-out duration-500"
                  >
                    view all
                  </a>
                </div>
              </div>
            </div>

            <div className="dropdown relative mr-5 md:static">
              <button
                onClick={() => this.toggleProp('messages')}
                className="text-gray-500 menu-btn p-0 m-0 hover:text-gray-900 focus:text-gray-900 focus:outline-none transition-all ease-in-out duration-300"
              >
                <CommentBank className="h-6 w-6" />
              </button>

              <button
                onClick={() => this.toggleProp('messages')}
                className={`fixed top-0 left-0 z-10 w-full h-full menu-overflow ${
                  this.state.messages ? '' : 'hidden'
                }`}
              ></button>

              <div
                className={`menu md:w-full md:right-0 rounded bg-white shadow-md absolute z-20 right-0 w-84 mt-5 py-2 animated faster ${
                  this.state.messages ? 'fadeIn' : 'hidden'
                }`}
              >
                <div className="px-4 py-2 flex flex-row justify-between items-center capitalize font-semibold text-sm">
                  <h1>messages</h1>
                  <div className="bg-teal-100 border border-teal-200 text-teal-500 text-xs rounded px-1">
                    <strong>3</strong>
                  </div>
                </div>
                <hr />

                <a
                  className="flex flex-row items-center justify-start px-4 py-4 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <div className="w-10 h-10 rounded-full overflow-hidden mr-3 bg-gray-100 border border-gray-300">
                    <img
                      className="w-full h-full object-cover"
                      src={User1Src}
                      alt=""
                    />
                  </div>

                  <div className="flex-1 flex flex-rowbg-green-100">
                    <div className="flex-1">
                      <h1 className="text-sm font-semibold">mohamed said</h1>
                      <p className="text-xs text-gray-500">yeah i know</p>
                    </div>
                    <div className="text-right text-xs text-gray-500">
                      <p>4 min ago</p>
                    </div>
                  </div>
                </a>
                <hr />

                <a
                  className="flex flex-row items-center justify-start px-4 py-4 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <div className="w-10 h-10 rounded-full overflow-hidden mr-3 bg-gray-100 border border-gray-300">
                    <img
                      className="w-full h-full object-cover"
                      src={User1Src}
                      alt=""
                    />
                  </div>

                  <div className="flex-1 flex flex-rowbg-green-100">
                    <div className="flex-1">
                      <h1 className="text-sm font-semibold">sull goldmen</h1>
                      <p className="text-xs text-gray-500">for sure</p>
                    </div>
                    <div className="text-right text-xs text-gray-500">
                      <p>1 day ago</p>
                    </div>
                  </div>
                </a>
                <hr />

                <a
                  className="flex flex-row items-center justify-start px-4 py-4 block capitalize font-medium text-sm tracking-wide bg-white hover:bg-gray-200 transition-all duration-300 ease-in-out"
                  href="#body"
                  onClick={consoleLog}
                >
                  <div className="w-10 h-10 rounded-full overflow-hidden mr-3 bg-gray-100 border border-gray-300">
                    <img
                      className="w-full h-full object-cover"
                      src={User1Src}
                      alt=""
                    />
                  </div>

                  <div className="flex-1 flex flex-rowbg-green-100">
                    <div className="flex-1">
                      <h1 className="text-sm font-semibold">mick</h1>
                      <p className="text-xs text-gray-500">is typing ....</p>
                    </div>
                    <div className="text-right text-xs text-gray-500">
                      <p>31 feb</p>
                    </div>
                  </div>
                </a>

                <hr />
                <div className="px-4 py-2 mt-2">
                  <a
                    href="#body"
                    onClick={consoleLog}
                    className="border border-gray-300 block text-center text-xs uppercase rounded p-1 hover:text-teal-500 transition-all ease-in-out duration-500"
                  >
                    view all
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
