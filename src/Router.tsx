import React, { Component } from 'react';
import { Route, Routes } from 'react-router-dom';

import Home from '@/views/Home';
import Empty from '@/views/Empty';

export default class Router extends Component<{}> {
  public render (): JSX.Element {
    return (
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/empty" element={<Empty />} />
      </Routes>
    )
  }
}