import React, { Component } from 'react';
import { SidebarNavLink, SidebarNavHeading, ClassNames } from '@/views/layouts/StyledComponents';
import {
  Analytics,
  Store,
  Mail
} from '@styled-icons/material-twotone';

export default class Sidebar extends Component<{}> {
  
  public render(): JSX.Element {
    return (
      <div id="sideBar" className={ClassNames.sidebar}>
        <div className="flex flex-col">
          <div className="text-right hidden md:block mb-4">
            <button id="sideBarHideBtn">
              <i className="fad fa-times-circle"></i>
            </button>
          </div>

          <SidebarNavHeading>homes</SidebarNavHeading>
          <SidebarNavLink to="/">
            <Analytics className={ClassNames.sidebarLinkIcon} />
            Analytics dashboard
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            ecommerce dashboard
          </SidebarNavLink>
          
          <SidebarNavHeading>apps</SidebarNavHeading>
          <SidebarNavLink to="/">
            <Mail className={ClassNames.sidebarLinkIcon} />
            email
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            chat
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            todo
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            calendar
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            invoice
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            file manager
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            file manager
          </SidebarNavLink>

          <SidebarNavHeading>UI Elements</SidebarNavHeading>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            typography
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            alerts
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            buttons
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            Content
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            colors
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            icons
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            card
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            Widgets
          </SidebarNavLink>
          <SidebarNavLink to="/">
            <Store className={ClassNames.sidebarLinkIcon} />
            Components
          </SidebarNavLink>
        </div>
      </div>
    );
  }
}
