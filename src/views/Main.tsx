import React, { FC } from 'react';
import Navbar from '@/views/layouts/Navbar';
import Sidebar from '@/views/layouts/Sidebar';
import Router from '@/Router';

const Main: FC = (): JSX.Element => {
  return (
    <main className="tracorona-app__body">
      <Navbar />

      <div className="tracorona-app__wrapper h-screen flex flex-row flex-wrap">
        <Sidebar />

        <div className="tracorona-app__container bg-gray-100 flex-1 p-6 md:mt-16"> 
          <Router />
        </div>
      </div>
    </main>
  )
}

export default Main;