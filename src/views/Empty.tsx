import React, { Component } from 'react';

export default class Empty extends Component<{}> {
  public render (): JSX.Element {
    return (
      <h1>Empty</h1>
    )
  }
}