import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const SidebarNavLink = styled(Link).attrs(() => ({
  activeClassName: 'text-teal-600',
  className: 'mb-3 capitalize font-medium text-sm hover:text-teal-600 transition ease-in-out duration-500'
}))`
  /* ... */
`

export const SidebarNavHeading = styled.p.attrs({
  className: 'uppercase text-xs text-gray-600 mb-4 mt-4 tracking-wider'
})`
  /* ... */
`;

export const ClassNames = {
  header: 'md:fixed md:w-full md:top-0 md:z-20 flex flex-row flex-wrap items-center bg-white px-6 py-3 border-b border-gray-300',
  navbar: 'animated md:hidden md:fixed md:top-0 md:w-full md:left-0 md:mt-16 md:border-t md:border-b md:border-gray-200 md:p-10 md:bg-white flex-1 pl-3 flex flex-row flex-wrap justify-between items-center md:flex-col md:items-center',
  navbarIcon: 'h-6 w-6',
  sidebar: 'relative flex flex-col flex-wrap bg-white border-r border-gray-300 p-6 flex-none w-64 md:-ml-64 md:fixed md:top-0 md:z-30 md:h-screen md:shadow-xl animated faster',
  sidebarLinkIcon: 'h4 w-5 mr-2'

}